from django.urls import path
from .views import sales_record_list, api_list_customers, api_list_salesperson, api_unsold_vehicle

urlpatterns = [
    path("sales/", sales_record_list, name="sales_record_list"),
    path("sales/customers/", api_list_customers, name="api_list_customers"),
    path("sales/employees/", api_list_salesperson, name="api_list_salesperson"),
    path("sales/<int:pk>/", sales_record_list, name="sales_record_list"),
    path("sales/employees/<int:sales_person_id>/", sales_record_list, name="sales_record_list"),
    path("sales/unsold-vehicles/", api_unsold_vehicle, name="api_unsold_vehicle")
]
