from django.shortcuts import render
from .models import SaleRecord, SalesPerson, Customer, VehicleVO
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json

class VehicleVOEncoder(ModelEncoder):
    model = VehicleVO
    properties = ["vin", "import_href"]

class SaleRecordListEncoder(ModelEncoder):
    model = SaleRecord
    properties = ["sales_person", "customer", "vin", "sale_price"]
    encoders = {"vin": VehicleVOEncoder()}

    def get_extra_data(self, o):
        return {"sales_person": o.sales_person.name, "customer": o.customer.name}

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = ["name", "address", "phonenumber"]

class SalesPersonListEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["name", "employeeid", "id"]



@require_http_methods(["GET", "POST"])
def sales_record_list(request, salerecord_vo_id=None, sales_person_id=None):
    if request.method == "GET":
        if salerecord_vo_id is not None:
            recordlist = SaleRecord.objects.filter(vin=salerecord_vo_id)
        elif sales_person_id is not None:
            recordlist = SaleRecord.objects.filter(sales_person=sales_person_id)
        else:
            recordlist = SaleRecord.objects.all()
        return JsonResponse(
            {"recordlist": recordlist},
            encoder=SaleRecordListEncoder
        )
    else:
        content = json.loads(request.body)
        import_href = f'/api/automobiles/{content["vin"]}/'
        vehicle = VehicleVO.objects.get(import_href=import_href)
        content["customer"] = Customer.objects.get(name=content["customer"])
        content["vin"] = VehicleVO.objects.get(vin=content["vin"])
        content["sales_person"] = SalesPerson.objects.get(name=content["sales_person"])

        salerecord = SaleRecord.objects.create(**content)
        return JsonResponse(
            salerecord,
            encoder = SaleRecordListEncoder,
            safe=False,
        )

@require_http_methods(["GET", "POST"])
def api_list_customers(request, salerecord_vo_id=None):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerListEncoder
        )
    else:
        content = json.loads(request.body)
        print(content)
        try:
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerListEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=400,
            )

@require_http_methods(["GET", "POST"])
def api_list_salesperson(request, salerecord_vo_id=None):
    if request.method == "GET":
        salesperson = SalesPerson.objects.all()
        return JsonResponse(
            {"salesperson": salesperson},
            encoder=SalesPersonListEncoder
        )
    else:
        content = json.loads(request.body)
        print(content)
        try:
            salesperson = SalesPerson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalesPersonListEncoder,
                safe=False
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Sales person does not exist"},
                status=400,
            )
    
@require_http_methods(["GET"])
def api_unsold_vehicle(request):
    if request.method == "GET":
        sold_vehicles = [recordlist.vin.vin for recordlist in SaleRecord.objects.all()]
        unsold_vehicles = VehicleVO.objects.exclude(vin__in=sold_vehicles)
        print(unsold_vehicles)
        return JsonResponse(
            {"automobiles": unsold_vehicles},
            encoder=VehicleVOEncoder,
            safe=False
        )