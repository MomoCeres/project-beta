# Car Services
Is an application for managing aspects of an automobile dealership, specifically its inventory, service center, and sales.

Team:

* Kate - Services Microservice
* Cooper - Sales

## Initialization
Run the following commands in your terminal after you have cloned the project from the main branch


1. `docker volume create beta-data` (Builds a new volume that the containers can consume and store data in)

2. `docker compose build` (Builds images as per the docker-compose.yml file)

3. `docker compose up` (Starts or restarts all services defined in the docker-compose.yml)

4. Open browser to http://localhost:3000 to view the home page.

## Service microservice

Kate Quashie-Javellana

## Sales microservice

Cooper Broderick
