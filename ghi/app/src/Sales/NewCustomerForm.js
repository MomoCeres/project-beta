import React from 'react';

class NewCustomerForm extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          name: '',
          address: '',
          phonenumber: '',
        };
      }
      handleChange = event => {
        const {name, value} = event.target;
        this.setState({
            [name]: value
        });
        }
      handleSubmit = async event => {
        event.preventDefault();
        const data = {...this.state};
        const customerUrl = `http://localhost:8090/api/sales/customers/`;
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
          const cleared = {
            name: '',
            address: '',
            phonenumber: ''
          };
          this.setState(cleared);
        }
      }

    render(){
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Please add a Customer</h1>
                <form onSubmit={this.handleSubmit} id="create-customer-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChange} value={this.state.name} placeholder="Customer Name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChange} value={this.state.address} placeholder="Customer Address" required type="text" name="address" id="address" className="form-control" />
                    <label htmlFor="address">Customer Address</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChange} value={this.state.phonenumber} placeholder="Customer Phone Number" required type="text" name="phonenumber" id="phonenumber" className="form-control" />
                    <label htmlFor="phone_number">Customer Phone Number</label>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        )
    }
}

export default NewCustomerForm
