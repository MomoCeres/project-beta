import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ServiceForm from './ServiceForm';
import ServiceHistory from './ServiceHistory';
import ListServices from './ServicesList';
import TechnicianForm from './TechnicianForm';
import SalesRecord from "./Sales/RecordList";
import NewCustomerForm from './Sales/NewCustomerForm';
import NewSalesPersonForm from './Sales/NewSalesmanForm';
import NewSaleForm from './Sales/NewSaleForm';
import SalesRecordbyPerson from './Sales/SalesRecordbyPerson';
import AddManufacturer from './Inventory/ManufacturerForm';
import AddModel from './Inventory/ModelForm';
import AddAutomobile from './Inventory/AutomobileForm';
import AutomobileList from './Inventory/AutomobileList';
import ManufacturerList from './Inventory/ManufacturerList';
import ModelList from './Inventory/ModelList';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/sales" element={<SalesRecord />} />
          <Route path="/customer" element={<NewCustomerForm />} />
          <Route path="/employees" element={<NewSalesPersonForm />} />
          <Route path="/newsale" element={<NewSaleForm />} />
          <Route path="/employeelog" element={<SalesRecordbyPerson />} />
          <Route path="/addmanufacturer" element={<AddManufacturer />} />
          <Route path="/addmodel" element={<AddModel />} />
          <Route path="/addvehicle" element={<AddAutomobile />} />
          <Route path="/listvehicle" element={<AutomobileList />} />
          <Route path="/listmanufacturers" element={<ManufacturerList />} />
          <Route path="/listmodels" element={<ModelList />} />
          <Route path="/services/new" element={<ServiceForm />} />
          <Route path="/services/" element={<ListServices />} />
          <Route path="/technicians/new" element={<TechnicianForm />} />
          <Route path="/services/history" element={<ServiceHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
